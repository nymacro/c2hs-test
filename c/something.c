#include <stdlib.h>
#include <stdio.h>

#include "something.h"

int something_new(something_t **self) {
    something_t *tmp = malloc(sizeof(something_t));
    if (tmp != NULL) {
        tmp->x = 100;
        *self = tmp;
        return ERROR_NONE;
    }
	return ERROR_ERROR;
}

void something_free(something_t *self) {
	if (!self) {
		return;
	}
	free(self);
}

int something_get(something_t *self, int *val) {
	if (!self) {
		return ERROR_ERROR;
	}
	*val = self->x;
	return ERROR_NONE;
}
