#ifndef SOMETHING_H
#define SOMETHING_H

#define ERROR_NONE  0
#define ERROR_ERROR 1

struct something_t {
    int x;
};
typedef struct something_t something_t;

int something_new(something_t **self);
void something_free(something_t *self);
int something_get(something_t *self, int *val);

#endif

