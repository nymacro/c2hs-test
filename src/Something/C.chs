{-# LANGUAGE ForeignFunctionInterface #-}
module Something.C where

import Data.Int
import Foreign.C
import Foreign.C.Types
import Foreign.Marshal       
import Foreign.Storable       
import Foreign.Ptr       

#include <something.h>

data Error = None
           | Error Int32
           deriving (Show, Eq)

toError (CInt 0) = None     
toError (CInt x) = Error x

fromError None = CInt 0
fromError (Error x) = CInt x

errorToEither (e, x) = case e of
                         None     -> Right x
                         Error ec -> Left  ec

{#fun something_new as newSomething'
      { alloca- `Ptr ()' peek* } -> `Error' toError #}
newSomething = fmap errorToEither newSomething'

{#fun something_free as freeSomething
      { `Ptr ()' } -> `()' #}

{#fun something_get as getSomething'
      { `Ptr ()', alloca- `CInt' peek* } -> `Error' toError #}
getSomething x = fmap errorToEither $ getSomething' x
