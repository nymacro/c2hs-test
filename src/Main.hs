module Main where

import           Something.C

main :: IO ()
main = do
  Right x <- newSomething
  i <- getSomething x
  print i
  freeSomething x
